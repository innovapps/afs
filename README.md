Información de librerías usadas en el projecto.

Las librerías se encuentran en la carpeta /assets/vendor, por lo que el proyecto no se conecta de forma remota a ningun CDN para descargar recursos que se usarán, es decir todo se hace de manera local en el proyecto.

assets/vendor/aos => Librería de animación para componentes y elementos en el website
assets/vendor/bootstrap => Framwework css para estilos de maquetación
assets/vendor/bootrstap-icons/ => Librería para importar las fuentes de iconos que se usan en el website
assets/vendor/boxicons => Librería para importar otras fuentes de iconos que se usan en el website
