<?php
   // data sent in header are in JSON format
   header('Content-Type: application/json');
   // takes the value from variables and Post the data
   $name = $_POST['name_f'];
   $email = $_POST['email_f'];
   $telefono = $_POST['telefono_f'];
   $deuda = $_POST['deuda_f'];
   $postmessage = $_POST['message_f'];  
   $to = "gbravo511@gmail.com";
   $subject = "Formulario Contacto AFS";
   // Email Template
   $message = "<b>Nombre : </b>". $name ."<br>";
   $message .= "<b>Teléfono: </b>".$telefono."<br>";
   $message .= "<b>Email Address : </b>".$email."<br>";
   $message .= "<b>Deuda: </b>".$deuda."<br>";
   $message .= "<b>Message : </b>".$postmessage."<br>";

   $header = "From:"+$email+" \r\n";
   $header .= "MIME-Version: 1.0\r\n";
   $header .= "Content-type: text/html\r\n";
   $retval = mail ($to,$subject,$message,$header);
   // message Notification
   if( $retval == true ) {
      echo json_encode(array(
         'success'=> true,
         'message' => 'Gracias. Mensaje ha sido enviado'
      ));
   }else {
      echo json_encode(array(
         'error'=> true,
         'message' => 'Ha ocurrido un error, vuelva a intentarlo'
      ));
   }
?>